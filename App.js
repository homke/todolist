import {createMaterialTopTabNavigator, createAppContainer} from 'react-navigation' 
import React from 'react'
import {Icon} from 'native-base';

import HomeScreen from './components/HomeScreen.js';
import ExpoScreen from './components/ExpoScreen.js';
import NativeBaseScreen from './components/NativeBaseScreen.js';


const AppTabNavigator = createMaterialTopTabNavigator({
  Expo: {
    screen: ExpoScreen,
    navigationOptions:{
      tabBarLabel: 'Ukończone',
      tabBarIcon: ({tintColor}) => (
        <Icon name='md-checkmark' color={tintColor} size={2} />
      )
    }
  },
  Home:{
      screen: HomeScreen,
      navigationOptions:{
        tabBarLabel: 'Do zrobienia',
        tabBarIcon: ({tintColor}) => (
          <Icon name='md-close' color={tintColor} size={2} />
        )
      }
  },
  NativeContBase:{
      screen: NativeBaseScreen,
      navigationOptions:{
        tabBarLabel: 'Dodaj',
        tabBarIcon: ({tintColor}) => (
          <Icon name='md-add' color={tintColor} size={2} />
        )
      }
  }
},{
  initialRouteName: 'Home',
  tabBarPosition: 'bottom',
  tabBarOptions: {
    showIcon: true,
    activeTintColor: '#3B5998',
    inactiveTintColor: 'grey',
    style:{
      backgroundColor:'white',
      borderTopColor: 'grey',
      borderTopWidth: 0.5
    },
    indicatorStyle:{
      height: 0
    },
  }
 
});

const AppContainer = createAppContainer(AppTabNavigator);

export default AppContainer;


