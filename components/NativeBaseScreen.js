import React, { useState } from 'react';
import { StyleSheet, Text, TextInput, AsyncStorage, ToastAndroid, Dimensions } from 'react-native';
import { Icon, Container, Header, Content, Left, Body, Right, Footer, Button } from 'native-base';
import { getStatusBarHeight } from 'react-native-status-bar-height';

import moment from 'moment';
import { pl } from 'moment/locale/pl'

const NativeBaseScreen = ({ navigation }) => {
    moment.locale(pl);

    const [author, setAuthor] = useState('');
    const [picture, setPicture] = useState('');
    const [content, setContent] = useState('');

    const onSubmit = async () => {
        const value = await AsyncStorage.getItem('@articles');
        const arrayOfArticles = value ? JSON.parse(value) : [];
        
        if (!picture.toLowerCase().includes('http')) {
            ToastAndroid.show('Musisz podać poprawny adres obrazka!', ToastAndroid.SHORT);
            return false;
        }

        arrayOfArticles.push({
            author,
            picture,
            content,
            date: moment().format('LLL'),
        });

        try {
            await AsyncStorage.setItem('@articles', JSON.stringify(arrayOfArticles));
            navigation.navigate('Home');
            clearState();
        } catch (error) {
            console.log(error);
        }
    }

    const clearState = () => {
        setAuthor('');
        setPicture('');
        setContent('');
    }

    return (
        <Container>
            <Header style={styles.header}>
                <Left style={styles.left}>
                    <Icon name='ios-bookmarks' />
                    <Text>Dodaj zadanie</Text>
                </Left>
                <Body />
                <Right />
            </Header>
            <Content>
                <Body style={styles.body}>
                    <TextInput style={styles.textInput} placeholder="Autor" value={author} onChangeText={text => setAuthor(text)} />
                    <TextInput style={styles.textInput} placeholder="URL zdjęcia" value={picture} onChangeText={url => setPicture(url)} />
                    <TextInput style={styles.textInput} placeholder="Treść" value={content} onChangeText={content => setContent(content)} numberOfLines={6} multiline />

                    <Icon name='md-add' onPress={onSubmit} size={20} style={{paddingTop: 20}}/>
                </Body>
            </Content>
        </Container>
    );
}



export default NativeBaseScreen;

var { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        paddingTop: getStatusBarHeight(),
        height: 54 + getStatusBarHeight(),
        backgroundColor: 'white',
    },
    left: {
        flex: 2,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    content: {
        backgroundColor: 'white'
    },
    textInput: {
        borderColor: 'gray', 
        borderWidth: 1,
        width: width- 15,
        textAlign: 'center',
        paddingTop: 5,
        paddingBottom: 5
    }
});