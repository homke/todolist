import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, AsyncStorage, ToastAndroid } from 'react-native';
import { uniqueId, isEqual, sortBy } from 'lodash';
import { Icon, Container, Header, Content, Left, Body, Right, Spinner } from 'native-base';
import { getStatusBarHeight } from 'react-native-status-bar-height';

import CardComponent from './CardComponent.js';
import moment from 'moment';

const HomeScreen = ({ navigation }) => {

  const [articles, setArticles] = useState();

  const sortByDate = (article) => {
    return -parseInt(moment(article.date, 'LLL').valueOf());
  };

  useEffect(() => {
    const fetchArticles = async () => {
      try {
        const value = await AsyncStorage.getItem('@articles');
        const arrayOfArticles = value ? JSON.parse(value) : [];
        
        setArticles(sortBy(arrayOfArticles.filter(article => !article.isComplete), sortByDate));
      } catch (error) {
        console.log(error);
      }
    }

    fetchArticles();

    const willFocusSubscription = navigation.addListener('willFocus', fetchArticles);

    return () => (
      willFocusSubscription.remove()
    );

  }, []);

  const onDelete = async (articleToDelete) => {    
    const newArticles = [...articles].filter(article => !isEqual(article, articleToDelete));
    await setArticles(newArticles);
    await onSave(newArticles);
    ToastAndroid.show('Usunięto poprawnie!', ToastAndroid.SHORT);
  }

  const onSave = async (articlesToSave) => {
    try {
        await AsyncStorage.setItem('@articles', JSON.stringify(articlesToSave));
    } catch (error) {
        console.log(error);
    }
  }

  const toogleCheck = async (sourceArticle) => {
    const newArticles = [...articles];
    const thisArticle = newArticles.find(article => isEqual(article, sourceArticle));
    thisArticle.isComplete = !thisArticle.isComplete;
    
    await setArticles([...newArticles].filter(article => !article.isComplete));
    await onSave(newArticles);
    ToastAndroid.show('Zadanie zakończone!', ToastAndroid.SHORT);
  }

  return (
    <Container>
      <Header style={styles.header}>
        <Left style={styles.left}>
          <Icon name='ios-bookmarks' />
          <Text>Twoje zadania ({articles ? articles.length : 0})</Text>
        </Left>
        <Body />
        <Right />
      </Header>
      <Content>
        {articles ? articles.map(article => (
          <CardComponent
            key={uniqueId('article_')}
            article={article}
            onDelete={onDelete}
            toogleCheck={toogleCheck}
            active={article.isComplete}
          />
        )) : <Spinner />}
      </Content>
    </Container>
  )
}

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    paddingTop: getStatusBarHeight(),
    height: 54 + getStatusBarHeight(),
    backgroundColor: 'white',
  },
  left: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});